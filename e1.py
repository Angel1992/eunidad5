# Escribe un programa que lea repetidamente números hasta que el usuario introduzca "fin”.

#__Autor:_ Angel Bolivar Contento Guamán
#__Email__ angel.b.contento@unl.edu.ec

cont = 0
total = 0

while True:
    valor = input("digite un número entero (o digite 'fin' para finalizar): ")
    if valor.lower() in "fin":
        break
    try:
        total += int (valor)
        cont  += 1
        media = total/cont
    except ValueError:
        print("Digito introducido incorrecto. Intenta nuevamente...")

print("El total es: ", total)
print(" Has ingresado : ", cont, " numeros")
print("La media aritmética de los valores es: ", float(media), "=", int(media))
